package visitors 
{
	import units.Armored;
	import units.Light;

	public class StatsVisitor implements IVisitor 
	{
		
		public var groupMaxHitPoints:Number = 0;
		public var groupHitPoints:Number = 0;
		
		/* INTERFACE visitors.IVisitor */
		
		public function visitLight(light:Light):void 
		{
			groupMaxHitPoints += light.maxHitPoints;
			groupHitPoints += light.hitPoints;
		}
		
		public function visitArmored(armored:Armored):void 
		{
			groupMaxHitPoints += armored.maxHitPoints;
			groupHitPoints += armored.hitPoints;			
		}
		
		public function print():void
		{
			trace ("Total", groupHitPoints, "/", groupMaxHitPoints);
		}
		
	}

}