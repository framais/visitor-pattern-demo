package visitors 
{
	import visitors.IVisitor;
	import units.Light;
	import flash.display.Sprite;
	import units.Armored;
	
	public class SiegeTankBullet extends Sprite implements IVisitor
	{
		public function SiegeTankBullet() 
		{
			graphics.beginFill(0x000000);
			graphics.drawRect( -2, -2, 4, 4);
			graphics.endFill();			
		}		
		
		public function visitLight(light:Light):void 
		{
			light.hitPoints -= 21;
		}
		
		public function visitArmored(armored:Armored):void 
		{
			armored.hitPoints -= 34;
		}
	}
}