package visitors 
{
	import units.Light;
	import units.Armored;
	public interface IVisitor 
	{		
		function visitLight(light:Light):void;
		function visitArmored(armored:Armored):void;	
	}
}