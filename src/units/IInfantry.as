package units
{
	import visitors.IVisitor;
	
	public interface IInfantry 
	{		
		function get maxHitPoints():Number;
		function get hitPoints():Number;
		function set hitPoints(value:Number):void;
		function get x():Number;
		function get y():Number;
		
		function accept(bullet:IVisitor):void;
	}	
}